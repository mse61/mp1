// ======== NAVBAR ACTIVE HIGHLIGHT FUNCTIONALITY ========
// REFERENCES: https://codepen.io/dbilanoski/pen/LabpzG
// NOTES: Since this method finds the corresponding navbar page link by searching for
// a matching section id, it's essential that each section with an id has a corresponding
// navbar link that uses that id as the href (necessary for one-page navigation anyway).

// Get all sections that have an ID defined
const sections = document.querySelectorAll("section[id]");

function navActive() {
    // For each of those sections, check if the section is within the viewframe
    sections.forEach((current) => {
        // Number of pixels to check above section top (instead of checking exact top)
        const tolerance = 200;

        // Get section id, calculate start point and end point
        const sectionId = current.getAttribute("id");
        const sectionStart = current.offsetTop - tolerance;
        const sectionEnd = sectionStart + current.offsetHeight;

        // Check if scroll pos is within the section start and end points
        if (window.scrollY > sectionStart && window.scrollY <= sectionEnd) {
            document
                .querySelector(`#navbar a[href*=${sectionId}]`)
                .classList.add("active");
        } else {
            document
                .querySelector(`#navbar a[href*=${sectionId}]`)
                .classList.remove("active");
        }
    });
}

// Scrolling event listener to handle active class in navbar
window.addEventListener("scroll", navActive);

// ======== NAVBAR SHRINK FUNCTIONALITY ========
// REFERENCES: N/A
// NOTES: N/A

function navShrink() {
    // Number of pixels before navbar should be shrunk
    const shrinkAt = 0;

    // If viewframe has passed the shrink point, shrink the navbar
    if (window.scrollY > shrinkAt) {
        document.getElementById("navbar").classList.add("shrink");
    } else {
        document.getElementById("navbar").classList.remove("shrink");
    }
}

// Scrolling event listener to handle active class in navbar
window.addEventListener("scroll", navShrink);

// ======== MODAL FUNCTIONALITY ========
// REFERENCES: https://codepen.io/geoffgraham/pen/LogERe
// NOTES: N/A

const projects = document.querySelectorAll(".project");
const closeBtns = document.querySelectorAll(".close");
const overlay = document.querySelector("#overlay");
let scrollY = 0;
let currProject = null;
let currModal = null;

function showModal(e) {
    // If modal click triggered this, ignore the function call
    if (
        !(
            e.target.classList.contains("overview") ||
            e.target.parentElement.classList.contains("overview")
        )
    ) {
        return;
    }

    // Show overlay (visibility necessary for interaction with clicks)
    overlay.style.visibility = "visible";
    overlay.style.opacity = "50%";

    // Update currently active project and modal (currentTarget to get handler element not inner elements)
    currProject = e.currentTarget;
    currModal = currProject.querySelector(".modal");

    // Disable hover effect on parent project
    currProject.style.cursor = "none";
    currProject.style.width = "200px";
    currProject.style.height = "200px";

    // Show the modal
    currModal.style.visibility = "visible";
    currModal.style.transform = "translate(-50%, -50%)";

    // Disable body scroll (fixed causes snap to top, so we have to offset top)
    document.body.style.position = "fixed";
    document.body.style.top = `-${scrollY}px`;
}

function hideModal() {
    if (currModal !== null) {
        // Hide the modal (visibility necessary for interaction with clicks)
        currModal.style.visibility = "hidden";
        currModal.style.transform = "translate(-50%, -70%)";

        // Hide overlay
        overlay.style.opacity = "0%";
        overlay.style.visibility = "hidden";

        // Re-enable hover effect on parent project
        currProject.style.cursor = "";
        currProject.style.width = "";
        currProject.style.height = "";

        // Update currently active modal and project
        currModal = null;
        currProject = null;

        // Remove top offset and scroll back to correct Y (without smooth scrolling)
        const oldY = document.body.style.top;
        document.body.style.position = "";
        document.body.style.top = "";
        document.documentElement.style.scrollBehavior = "auto";
        window.scrollTo(0, parseInt(oldY, 10) * -1);
        document.documentElement.style.scrollBehavior = "smooth";
    }
}

// Click event listeners for projects, close buttons, and overlay
projects.forEach((project) => project.addEventListener("click", showModal));
closeBtns.forEach((closeBtn) => closeBtn.addEventListener("click", hideModal));
overlay.addEventListener("click", hideModal);

// Scroll event listener to keep track of the window Y position
window.addEventListener("scroll", () => {
    scrollY = window.scrollY;
});

// // ======== CAROUSEL FUNCTIONALITY ========
// // REFERENCES: https://medium.com/allenhwkim/how-to-build-a-carousel-in-pure-javascript-98d758a18811
// // NOTES: N/A

const carousels = document.querySelectorAll(".carousel");
const carouselIndices = Array(carousels.length).fill(0);

function updateCarousel(e) {
    // Determine increment direction based on target
    const button = e.target;
    let increment = 1;

    if (button.classList.contains("prev")) {
        // Need to decrement if prev button pressed
        increment = -1;
    } else if (!button.classList.contains("next")) {
        // Button was neither a prev nor next button, so ignore the event
        return;
    }

    // Get carousel, calculate its current slide index and max slide index
    const carousel = e.currentTarget;
    const slides = carousel.querySelectorAll(".slide");
    const carouselIdx = Array.from(carousels).indexOf(carousel);
    const slideIdx = carouselIndices[carouselIdx];
    const maxIndex = slides.length - 1;

    // Calculate new slide index
    let newIndex = slideIdx + increment;

    // Wrap index if necessary
    if (newIndex < 0) {
        newIndex = maxIndex;
    } else if (newIndex > maxIndex) {
        newIndex = 0;
    }

    // Shrink current slide, grow new slide, update current slide index
    slides[slideIdx].style.flex = "0";
    slides[newIndex].style.flex = "1";
    carouselIndices[carouselIdx] = newIndex;
}

// Click event listeners for carousels
carousels.forEach((carousel) =>
    carousel.addEventListener("click", updateCarousel)
);
